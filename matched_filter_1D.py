#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 14 21:15:49 2016

   This program filters a 1D array stored in the numpy array DATARRAY
   and filters it with a matched fiter for a profile template
   TPROF or a Gaussian shaped template profile with full width
   at half maximum FWHM (in pixel units). It has been tested to
   give a similar performance to the Fortran90
   implementation by D. Herranz. Running time has not been compared.

   Optionally, images can be padded (with a reflection of the
   data inside) in order to reduce border effects.
   This option is activated by default.

@author: herranz
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import time
import smooth1D

def matched_filter_1d(datarray,tprof=2.0,nbins=50,
                      topad=True,toplot=False,verbose=False,
                      tosmooth=False,lensmooth=5001):

    """
    Applies a matched filter to a 1D array using a specified template profile or a Gaussian profile.

    Parameters
    ----------
    datarray : numpy.ndarray
        The input data array to be filtered.
    tprof : float or numpy.ndarray, optional
        The template profile for the filter.
        If a float is provided, it is assumed to be the FWHM of a Gaussian profile.
        If a numpy array is provided, it is used directly as the template profile.
        Default is 2.0.
    nbins : int, optional
        The number of linear bins over which the power spectrum of the data
        is binned and smoothed. A typical value is DATARRAY.size // 50.
        Default is 50.
    topad : bool, optional
        Whether to pad the data array to reduce border effects.
        Default is True.
    toplot : bool, optional
        Whether to generate matplotlib plots for visualization.
        Default is False.
    verbose : bool, optional
        Whether to print information during the execution of the filter.
        Default is False.
    tosmooth : bool, optional
        Whether to apply additional smoothing to the data.
        Default is False.
    lensmooth : int, optional
        The length of the smoothing window if tosmooth is True.
        Default is 5001.

    Returns
    -------
    numpy.ndarray
        The filtered data array.

    Examples
    --------
    >>> import numpy as np
    >>> datarray = np.random.random(1000)
    >>> filtered_data = matched_filter_1d(datarray, tprof=5.0, toplot=True)

    Notes
    -----
    This implementation has been tested to give similar performance to the
    Fortran90 implementation by D. Herranz.

    """

    # Initialization:

    start_time = time.time()

    xdata = np.array(datarray.copy(),dtype='float32')
    N0    = xdata.size
    dN1   = 0
    dN2   = 0

    if topad:
        N  = 2**(np.int(np.log2(N0))+1)  # we will pad the data up to the size
                                         # of the next power of 2
        dN = N-N0
        if np.mod(dN,2)==0:
            dN1 = dN//2
            dN2 = dN//2
        else:
            dN1 = 1+dN//2
            dN2 = dN//2
        pattern = (dN1,dN2)
        xdata = np.pad(xdata,pattern,mode='reflect')
    else:
        N = N0

    c     = N // 2

    # Generation of the Gaussian profile (if needed):

    x = np.arange(0,N,1,dtype='float32')
    if np.size(tprof)==1:
        fwhm  = tprof
        xprof = np.exp(-4*np.log(2.0)*((x-c)**2)/fwhm**2)
        nbord = 2*np.int(fwhm)
    else:
        xprof = np.array(tprof,dtype='float32')
        nbord = 20

    t1 = time.time()

    # FFT of the data and the profile

    fdata = np.fft.fftshift(np.fft.fft(xdata))
    fpro0 = np.fft.fftshift(np.fft.fft(xprof))
    fprof = np.abs(fpro0)
    t2 = time.time()

    # Power spectrum estimation and smoothing:

    P     = np.real(np.conj(fdata)*fdata)

    binsz   = c/(nbins-1)
    bcenter = np.arange(0,nbins,1)*binsz
    bborder = bcenter+binsz/2
    Pmean   = np.zeros(nbins)
    dabs    = np.abs(x-c)
    for i in range(nbins):
        if i==0:
            mask = dabs<bborder[i]
        else:
            mask = (dabs>=bborder[i-1])&(dabs<bborder[i])
        Pmean[i] = P[mask].mean()
    finterp = interp1d(bcenter,Pmean,fill_value='extrapolate')
    Pmap    = finterp(dabs)
    t3 = time.time()

    # Definition of the filter. The filter at zero frequency is forced to zero
    # value in order to forcefully remove the mean value of the output filtered
    # array.

    filtro    = np.divide(fprof,Pmap)
    if tosmooth:
        if np.mod(lensmooth,2)==0:
            filtro = smooth1D.smooth(filtro.copy(),window_len=lensmooth+1)
        else:
            filtro = smooth1D.smooth(filtro.copy(),window_len=lensmooth)
    filtro[c] = 0.0

    # Normalization:

    ffiltg    = np.multiply(filtro,fpro0)
    filtg     = np.real(np.fft.ifft(np.fft.ifftshift(ffiltg)))
    norm      = 1.0/filtg.max()
    t4        = time.time()

    # Filtering step:

    ffdata    = np.multiply(filtro,fdata)
    filtrado  = norm*np.real(np.fft.ifft(np.fft.ifftshift(ffdata)))
    t5        = time.time()

    if dN2>0:
        filtrado = filtrado[dN1:-dN2] # go back to the original size
    else:
        filtrado = filtrado[dN1:]


    if toplot:  # PLOTS:

        t6 = time.time()
        x0 = np.arange(0,N0,1,dtype='float32')

        plt.figure()
        plt.subplot(2,2,1)
        if dN2>0:
            plt.plot(x0,xdata[dN1:-dN2])
        else:
            plt.plot(x0,xdata[dN1:])
        plt.axis('tight')
        plt.xlabel('x')
        plt.ylabel('TOD')
        plt.title('Input data')

        plt.subplot(2,2,2)
        plt.loglog((x-c)*(2*np.pi/x.size),P/(P.size**2),label='non smoothed')
        plt.loglog((x-c)*(2*np.pi/x.size),Pmap/(Pmap.size**2),'r',label='smoothed')
        plt.axis('tight')
        plt.xlabel('Angular Frequency')
        plt.ylabel('P')
        plt.legend()
        plt.title('Power Spectrum')

        plt.subplot(2,2,3)
        plt.plot(x0,filtrado)
        plt.axis('tight')
        plt.xlabel('x')
        plt.ylabel('Filtered TOD')
        plt.title('Filtered data')

        plt.subplot(2,2,4)
        plt.plot(x0,filtrado)
        plt.plot(x0,5*filtrado.std()*np.ones(x0.size),':',label='5 sigma')
        plt.plot(x0,4*filtrado.std()*np.ones(x0.size),':',label='4 sigma')
        plt.plot(x0,3*filtrado.std()*np.ones(x0.size),':',label='3 sigma')
        plt.xlabel('x')
        plt.ylabel('Filtered TOD')
        plt.legend()
        plt.title('Filtered data with thresholds')
        plt.xlim([x0.min(),x0.max()])

        plt.figure()
        plt.semilogx((x-c)*(2*np.pi/x.size),norm*filtro)
        plt.xlabel('Angular frequency')
        plt.ylabel('Filter')
        plt.axis('tight')

        t7 = time.time()

    if verbose:  # MESSAGES:
        t8 = time.time()
        print(' ')
        print(' ---- Input data size = {0}'.format(N0))
        if isinstance(tprof, float):
            print(' ---- Filter FWHM  = {0}'.format(tprof))
            print(' ---- Filter sigma = {0}'.format(0.42466090014400953*tprof))
        if topad:
            print(' ---- Input data padded to size = {0}'.format(N))
        print(' ---- Real space profile generated in {0} s'.format(t1-start_time))
        print(' ---- Initial FFTs in {0} s'.format(t2-t1))
        print(' ---- Pmap generated in {0} s'.format(t3-t2))
        print(' ---- Mormalization calculated in {0} s'.format(t4-t3))
        print(' ---- Output data size = {0}'.format(filtrado.size))
        print(' ---- Matched filtered in total {0} s'.format(t5-start_time))
        if toplot:
            print(' ---- Plots done in {0} s'.format(t7-t6))
        print(' ---- Total execution time = {0} s'.format(t8-start_time))
        print(' ---- SNR gain factor = {0}'.format(datarray.std()/filtrado.std()))

    return filtrado


def Gaussian_shape(N,centro,fwhm,amplitud):
    """
    Generates a 1D Gaussian profile.

    Parameters
    ----------
    N : int
        The length of the array.
    centro : float
        The center of the Gaussian peak.
    fwhm : float
        The full width at half maximum (FWHM) of the Gaussian.
    amplitud : float
        The amplitude of the Gaussian.

    Returns
    -------
    numpy.ndarray
        The Gaussian profile.

    Examples
    --------
    >>> gaussian = Gaussian_shape(100, 50, 10, 1.0)
    """
    x   = np.arange(0,N,1,dtype='float32')
    return  amplitud*np.exp(-4*np.log(2.0)*((x-centro)**2)/fwhm**2)
