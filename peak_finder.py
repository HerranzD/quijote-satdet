#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 11 10:37:04 2017

@author: herranz
"""

import numpy as np
import astropy.units as u
import astropy.io.ascii as asc
import matched_filter_1D as mf1
import peakutils as pu
import copy as cp

from astropy.table       import Table
from astropy.io          import fits
from astropy.coordinates import SkyCoord
from astropy.time        import Time
from scipy.interpolate   import interp1d
from matplotlib          import pyplot as plt
from collections         import deque
from peakutils.plot      import plot as pplot
from sancho_mfi2_io      import read_mfi2_btod3,write_mfi2_btod3


"""
    ----------------------------------------------------------------------------
     TO BE DELETED WHEN USED AT IAC: This defines a list of BTOD and
       AZ-STACK examples for tests made here at the IFCA by Diego
    ----------------------------------------------------------------------------
"""

froot          = '/Users/herranz/Trabajo/QUIJOTE/BTODS/Data/pour_diego/'
lista_ejemplos = froot+'file_list.txt'

ejs = asc.read(lista_ejemplos,data_start=0,names=['fichero'])
nej = np.size(ejs['fichero'])

ejemplos_btod = [froot+ejs['fichero'][i]+'.btod' for i in range(nej)]
ejemplos_stod = [froot+ejs['fichero'][i]+'.stack' for i in range(nej)]

# Ejemplos MFI2

dire = '/Users/herranz/Dropbox/Trabajo/QUIJOTE/BTODS/Data/2024/'
ejemplo_btod2 = dire + 'NOMINAL60-240418-1015.btod3'
ejemplo_tod2  = dire + 'NOMINAL60-240418-1015-0000.tod3'


"""
    ----------------------------------------------------------------------------
     Loads a table of MFI parameters form a FITS file:
    ----------------------------------------------------------------------------
"""

files_path     = '/Users/herranz/Trabajo/QUIJOTE/BTODS/'    # write here the path of the directory where the code is installed

MFI_table_name = files_path+'MFI_channels_info.fits'
hdulist        = fits.open(MFI_table_name)
MFI_data       = hdulist[1].data
hdulist.close()

# %%   BTOD3 class

class BTOD3:
    """
    A class to represent the BTOD3 data. The BTOD3 data is a dictionary
    containing the following keys: 'TIME', 'DATA', 'WEI', 'FLAG', 'GAINS',
    'POINTINGMODEL', 'AZHORN', 'ELHORN', 'SIGMACOV', 'MSBIN'. The 'TIME' key
    contains the time array, 'DATA' contains the data array, 'WEI' contains the
    weights array, 'FLAG' contains the flags array, 'GAINS' contains the gains
    array, 'POINTINGMODEL' contains the pointing model array, 'AZHORN' contains
    the azimuth of each horn array, 'ELHORN' contains the elevation of each horn
    array, 'SIGMACOV' contains the covariance matrix array, and 'MSBIN' contains
    the milliseconds per bin array.

    Attributes
    ----------
    tod_dicc : dict
        Dictionary containing the BTOD3 data.

    Methods
    -------
    __getitem__(sliced):
        Returns a new BTOD3 object with the sliced data.
    copy():
        Returns a copy of the BTOD3 object.
    to_btod:
        Converts the BTOD3 object to a BTOD object.
    nhorns:
        Returns the number of horns.
    time:
        Returns the julian date as float.
    jd:
        Returns the julian date as Time julian date.
    mjd:
        Returns the modified julian date as Time modified julian date.
    date:
        Returns the date and time as Time object.
    seconds:
        Returns the time since the start of the BTOD3 in seconds.
    size:
        Returns the size of the time array.
    az:
        Returns the azimuth in degrees.
    el:
        Returns the elevation in degrees.
    gl:
        Returns the galactic longitude in degrees.
    gb:
        Returns the galactic latitude in degrees.
    coord:
        Returns the sky coordinates.
    glon:
        Returns the galactic longitude in degrees. It is an alias for gl.
    glat:
        Returns the galactic latitude in degrees. It is an alias for gb.
    ra:
        Returns the right ascension in degrees.
    dec:
        Returns the declination in degrees.
    par:
        Returns the parallactic angle in degrees.
    data:
        Returns the data from the dictionary.
    nchans:
        Returns the number of data channels.
    wei:
        Returns the weights from the dictionary.
    flag:
        Returns the flags from the dictionary.
    msbin:
        Returns the milliseconds per bin from the dictionary.
    pmodel:
        Returns the pointing model from the dictionary.
    azhorn:
        Returns the azimuth of each horn from the dictionary in degrees.
    elhorn:
        Returns the elevation of each horn from the dictionary in degrees.
    sigmacov:
        Returns the covariance matrix from the dictionary.
    azimuth_step:
        Calculates the average absolute azimuth step between samples.
    azimuth_step_stddev:
        Calculates the standard deviation of the azimuth step between samples.
    channel(ichan):
        Returns the channel number for index ichan, from MFI_data.
    polarizer(ichan):
        Returns the polarizer for index ichan, from MFI_data.
    signal(ichan):
        Returns the signal for index ichan, from MFI_data.
    frequency(ichan):
        Returns the frequency in GHz for index ichan, from MFI_data.
    fwhm(ichan):
        Returns the FWHM in arcmin for index ichan, from MFI_data.
    mask(value='all'):
        Returns a boolean array with the mask of the data. If value is 'all',
        all data flagged with numbers other than 0 are masked. If not, only the
        data with flag value equal to value are masked.
    masked_indices(value='all'):
        Returns the indices of the masked data. If value is 'all', all data
        flagged with numbers other than 0 are masked. If not, only the data with
        flag value equal to value are masked.
    interp_data(flag_value='all'):
        Interpolates the data in the flagged samples. If flag_value is 'all',
        any flag value > 0 will be considered for the interpolation.
    match_filter(mask_flagged=True,flag_value='all',nbins=500,toplot=False,verbose=False,tosmooth=False,lensmooth=100,normalize=False):
        Applies a matched filter to the data. The matched filter is applied to
        each channel independently. The data can be interpolated in the flagged
        positions before applying the filter. If mask_flagged is True, the flagged
        data is masked out and replaced by interpolated data from the non-masked
        data. If flag_value is 'all', any flag value > 0 will be considered for the
        interpolation. If normalize is True, the matched filter signal is normalized.
    read(fname):
        Reads the BTOD dictionary from a file and returns a new BTOD3 object.
    write(filename):
        Writes the BTOD dictionary to a file.
    plot(galeq='galactic'):
        Plots the coordinates of the horns in Galactic or Equatorial coordinates.
    plot_chan(ichan,flag=None,threshold=None,newfig=True,xaxis='mjd'):
        Plots a single channel from a BTOD object. If flag is not None, it marks
        the flagged data. If threshold is not None, it marks the threshold value.
    detect_thr(ichan,sigma_threshold,toplot=False,closeplots=False):
        Detects the indexes of the data that exceed a threshold. If toplot is True,
        it plots the detected indexes. If closeplots is True, it closes all previous
        plot windows.
    add_flags(output_flag_value,sigma_threshold=3.0,use_filter=True,mask_flagged=True,flag_value='all',nbins=500,tosmooth=False,lensmooth=100):
        Flags the data that exceeds a threshold. The flagged data can be interpolated
        and/or filtered before applying the threshold. Flagging is done for each
        channel independently. If use_filter is True, the data is filtered before
        applying the threshold. By default, the input data for the filter is a version
        of the data with the already flagged samples replaced by interpolated data.
        If mask_flagged is True, the flagged data is masked out and replaced by
        interpolated data from the non-masked data. If flag_value is 'all', any flag
        value > 0 will be considered for the interpolation.
    """

    def __init__(self, tod_dicc):
        self.tod_dicc = tod_dicc

    def __getitem__(self,sliced):
        """
        Returns a new BTOD3 object with the sliced data.

        Parameters
        ----------
        sliced : slice
            Slice object specifying the data to be sliced.

        Returns
        -------
        BTOD3
            New BTOD3 object with the sliced data.
        """
        old_dicc = cp.deepcopy(self.tod_dicc)

        new_dicc = {}

        for key in old_dicc.keys():
            if isinstance(old_dicc[key],str):
                new_dicc[key] = old_dicc[key]
            elif isinstance(old_dicc[key],np.ndarray):
                if old_dicc[key].ndim == 1:
                    new_dicc[key] = old_dicc[key][sliced]
                elif old_dicc[key].ndim == 2:
                    new_dicc[key] = old_dicc[key][sliced,:]
                elif old_dicc[key].ndim == 3:
                    new_dicc[key] = old_dicc[key][sliced,:,:]
                elif old_dicc[key].ndim == 4:
                    new_dicc[key] = old_dicc[key][sliced,:,:,:]
            else:
                new_dicc[key] = old_dicc[key]

        return BTOD3(new_dicc)

    def copy(self):
        """
        Returns a copy of the BTOD3 object.

        Returns
        -------
        BTOD3
            Copy of the BTOD3 object.
        """
        new_dict = cp.deepcopy(self.tod_dicc)
        return BTOD3(new_dict)

    def to_btod(self):
        """
        Converts the BTOD3 object to a BTOD object.

        Returns
        -------
        BTOD
            BTOD object converted from BTOD3.
        """
        return self.tod_dicc['NHORNS']

    @property
    def nhorns(self):
        """
        Returns the number of horns.

        Returns
        -------
        int
            Number of horns.
        """
        return self.tod_dicc['NHORNS']

    @property
    def time(self):
        """
        Returns the julian date as float.

        Returns
        -------
        float
            Julian date.
        """
        return self.tod_dicc['JD']

    @property
    def jd(self):
        """
        Returns the julian date as Time julian date.

        Returns
        -------
        Time
            Julian date as Time julian date.
        """
        return Time(self.tod_dicc['JD'], format='jd')

    @property
    def mjd(self):
        """
        Returns the modified julian date as Time modified julian date.

        Returns
        -------
        Time
            Modified julian date as Time modified julian date.
        """
        return Time(self.tod_dicc['JD'], format='jd').mjd

    @property
    def date(self):
        """
        Returns the date and time as Time object.

        Returns
        -------
        Time
            Date and time as Time object.
        """
        return Time(self.tod_dicc['JD'], format='jd').datetime

    @property
    def seconds(self):
        """
        Returns the time since the start of the BTOD3 in seconds.

        Returns
        -------
        Quantity
            Time since the start of the BTOD3 in seconds.
        """
        t = self.time
        t = t-t[0]
        return t*u.day.to('second')

    @property
    def size(self):
        """
        Returns the size of the time array.

        Returns
        -------
        int
            Size of the time array.
        """
        return self.time.size

    @property
    def az(self):
        """
        Returns the azimuth in degrees.

        Returns
        -------
        Quantity
            Azimuth in degrees.
        """
        return self.tod_dicc['AZ']*u.deg

    @property
    def el(self):
        """
        Returns the elevation in degrees.

        Returns
        -------
        Quantity
            Elevation in degrees.
        """
        return self.tod_dicc['EL']*u.deg

    @property
    def gl(self):
        """
        Returns the galactic longitude in degrees.

        Returns
        -------
        Quantity
            Galactic longitude in degrees.
        """
        return self.tod_dicc['GL']*u.deg

    @property
    def gb(self):
        """
        Returns the galactic latitude in degrees.

        Returns
        -------
        Quantity
            Galactic latitude in degrees.
        """
        return self.tod_dicc['GB']*u.deg

    @property
    def coord(self):
        """
        Returns the sky coordinates.

        Returns
        -------
        SkyCoord
            Sky coordinates.
        """
        return SkyCoord(self.gl, self.gb, frame='galactic')

    @property
    def glon(self):
        """
        Returns the galactic longitude in degrees.

        Returns
        -------
        Quantity
            Galactic longitude in degrees.
        """
        return self.gl

    @property
    def glat(self):
        """
        Returns the galactic latitude in degrees.

        Returns
        -------
        Quantity
            Galactic latitude in degrees.
        """
        return self.gb

    @property
    def ra(self):
        """
        Returns the right ascension in degrees.

        Returns
        -------
        Quantity
            Right ascension in degrees.
        """
        return self.coord.icrs.ra

    @property
    def dec(self):
        """
        Returns the declination in degrees.

        Returns
        -------
        Quantity
            Declination in degrees.
        """
        return self.coord.icrs.dec

    @property
    def par(self):
        """
        Returns the parallactic angle in degrees.

        Returns
        -------
        Quantity
            Parallactic angle in degrees.
        """
        return self.tod_dicc['PAR']*u.deg

    @property
    def data(self):
        """
        Returns the data from the dictionary.

        Returns
        -------
        ndarray
            Data from the dictionary.
        """
        return self.tod_dicc['DATA']

    @property
    def nchans(self):
        """
        Returns the number of data channels.

        Returns
        -------
        int
            Number of data channels.
        """
        return self.data.shape[1]

    @property
    def wei(self):
        """
        Returns the weights from the dictionary.

        Returns
        -------
        ndarray
            Weights from the dictionary.
        """
        return self.tod_dicc['WEI']

    @property
    def flag(self):
        """
        Returns the flags from the dictionary.

        Returns
        -------
        ndarray
            Flags from the dictionary.
        """
        return self.tod_dicc['FLAG']

    @property
    def msbin(self):
        """
        Returns the milliseconds per bin from the dictionary.

        Returns
        -------
        ndarray
            Milliseconds per bin from the dictionary.
        """
        return self.tod_dicc['MSBIN']

    @property
    def pmodel(self):
        """
        Returns the pointing model from the dictionary.

        Returns
        -------
        ndarray
            Pointing model from the dictionary.
        """
        return self.tod_dicc['POINTINGMODEL']

    @property
    def azhorn(self):
        """
        Returns the azimuth of each horn from the dictionary in degrees.

        Returns
        -------
        Quantity
            Azimuth of each horn in degrees.
        """
        return self.tod_dicc['AZHORN']*u.deg

    @property
    def elhorn(self):
        """
        Returns the elevation of each horn from the dictionary in degrees.

        Returns
        -------
        Quantity
            Elevation of each horn in degrees.
        """
        return self.tod_dicc['ELHORN']*u.deg

    @property
    def sigmacov(self):
        """
        Returns the covariance matrix from the dictionary.

        Returns
        -------
        ndarray
            Covariance matrix from the dictionary.
        """
        return self.tod_dicc['SIGMACOV']

    @property
    def azimuth_step(self):
        """
        Calculates the average absolute azimuth step between samples.

        Returns
        -------
        Quantity
            Average absolute azimuth step in degrees.
        """
        azarr = self.az.to(u.deg).value
        daz   = azarr[1:]-azarr[:-1]
        npos  = np.count_nonzero(daz>0)
        nneg  = np.count_nonzero(daz<0)
        if npos>0:
            pdaz  = daz[daz>0]
            x     = np.abs(pdaz.mean())
        if nneg>0:
            ndaz  = daz[daz<0]
            y     = np.abs(ndaz.mean())
        z = np.min([x,y])
        return z*u.deg

    @property
    def azimuth_step_stddev(self):
        """
        Calculates the standard deviation of the azimuth step between samples.

        Returns
        -------
        Quantity
            Standard deviation of the azimuth step in degrees.
        """
        azarr = self.az.to(u.deg).value
        daz   = azarr[1:]-azarr[:-1]
        npos  = np.count_nonzero(daz>0)
        nneg  = np.count_nonzero(daz<0)
        if npos>0:
            pdaz  = daz[daz>0]
            x     = np.abs(pdaz.mean())
            sx    = pdaz.std()
        if nneg>0:
            ndaz  = daz[daz<0]
            y     = np.abs(ndaz.mean())
            sy    = ndaz.std()
        z = np.min([x,y])
        if z == x:
            return sx
        else:
            return sy

# %% -------  MFI2 channels -----------------------------------

    def channel(self,ichan):
        """
        Returns the channel number for index ichan, from MFI_data

        Parameters
        ----------
        ichan : int
            Channel number.

        Returns
        -------
        int
            The number assigned to the channel in the structure MFI_data.

        """
        return MFI_data['Channel'][ichan]

    def polarizer(self,ichan):
        """
        Returns the polarizer for index ichan, from MFI_data

        Parameters
        ----------
        ichan : int
            Channel number.

        Returns
        -------
        str
            The polarizer assigned to the channel in the structure MFI_data.

        """
        return MFI_data['Polarizer'][ichan]

    def signal(self,ichan):
        """
        Returns the signal for index ichan, from MFI_data

        Parameters
        ----------
        ichan : int
            Channel number.

        Returns
        -------
        str
            The signal assigned to the channel in the structure MFI_data.

        """
        return MFI_data['Signal'][ichan]

    def frequency(self,ichan):
        """
        Returns the frequency in GHz for index ichan, from MFI_data

        Parameters
        ----------
        ichan : int
            Channel number.

        Returns
        -------
        float
            The frequency assigned to the channel in the structure MFI_data.

        """
        return MFI_data['Frequency'][ichan]*u.GHz

    def fwhm(self,ichan):
        """
        Returns the FWHM in arcmin for index ichan, from MFI_data

        Parameters
        ----------
        ichan : int
            Channel number.

        Returns
        -------
        float
            The FWHM assigned to the channel in the structure MFI_data.

        """
        return MFI_data['FWHM'][ichan]*u.arcmin


# %% -------  MASKING -----------------------------------

    def mask(self,value='all'):
        """
        Returns a boolean array with the mask of the data.

        Parameters
        ----------
        value : int, optional
            Flag value to mask. If equal to 'all', all data flagged with numbers
            other than 0 are masked. If not, only the data with flag value
            equal to value are masked. Default is 'all'.
        """


        if value == 'all':
            return self.flag != 0
        else:
            return self.flag == value

    def masked_indices(self,value='all'):
        """
        Returns the indices of the masked data.

        Parameters
        ----------
        value : int, optional
            Flag value to mask. If equal to 'all', all data flagged with numbers
            other than 0 are masked. If not, only the data with flag value
            equal to value are masked. Default is 'all'.

        """

        z = np.where(self.mask(value=value))
        return Table([z[0],z[1]],names=['Time index','channel'])

# %% -------  DATA INTERPOLATION -----------------------------

    def interp_data(self,flag_value='all'):
        """
        Interpolates the data in the flagged samples.

        Parameters
        ----------
        flag_value : int, optional
            Flag value to interpolate. If flag_value=='all', any flag value > 0
            will be considered for the interpolation. Default is 'all'.

        Returns
        -------
        BTOD3
            A new BTOD3 object with the interpolated data.
        """

        t = self.time
        m = self.mask(value=flag_value)
        n = self.nchans
        v = self.copy()
        d = v.tod_dicc

        for ichan in range(n):

            ma = m[:,ichan] == False
            x0 = t[ma]
            yy = d['DATA'][:,ichan]
            y0 = yy[ma]

            f  = interp1d(x0,y0,bounds_error=False,fill_value='extrapolate')

            d['DATA'][:,ichan] = f(t)

        return BTOD3(d)

# %% -------  MATCHED FILTERING -----------------------------------

    def match_filter(self,mask_flagged=True,
                     flag_value = 'all',
                     nbins      = 500,
                     toplot     = False,
                     verbose    = False,
                     tosmooth   = False,
                     lensmooth  = 100,
                     normalize  = False):
        """
        Applies a matched filter to the data. The matched filter is applied to
        each channel independently. The data can be interpolated in the flagged
        positions before applying the filter.

        Parameters
        ----------
        mask_flagged : bool, optional
            If True, the flagged data is masked out and replaced by
            interpolated data from the non-masked data. Default is True.
        flag_value : int, optional
            Flag value that defines where to interpolate.
            If flag_value=='all', any flag value > 0 will be considered for the
            interpolation. Default is 'all'.
        nbins : int, optional
            Number of bins for the matched filter. Default is 500.
        toplot : bool, optional
            If True, plots the matched filter. Default is False.
        verbose : bool, optional
            If True, prints information about the matched filter. Default is False.
        tosmooth : bool, optional
            If True, the matched filter signal is smoothed. Default is False.
        lensmooth : int, optional
            Length of the smoothing window. Default is 100.

        Returns
        -------
        BTOD3
            A new BTOD3 object with the matched filter applied to the data.
        """

        if mask_flagged:
            filtered = self.interp_data(flag_value=flag_value)
        else:
            filtered = self.copy()

        for ichan in range(self.nchans):

            y         = filtered.data[:,ichan]
            fwhm      = (filtered.fwhm(ichan)/filtered.azimuth_step).si.value
            fy        = mf1.matched_filter_1d(y,tprof=fwhm,
                                              nbins=nbins,topad=True,
                                              toplot=toplot,
                                              verbose=verbose,
                                              tosmooth=tosmooth,
                                              lensmooth=lensmooth)

            if normalize:
                fy = (fy-fy.mean())/fy.std()

            filtered.data[:,ichan] = fy

        return filtered

# %% -------  INPUT/OUTPUT -----------------------------------

    def read(fname):
        """
        Reads the BTOD3 dictionary from a file and returns a new BTOD3 object.
        """
        toddicc = read_mfi2_btod3(fname)
        return BTOD3(toddicc)

    def write(self,filename):
        """
        Writes the BTOD3 dictionary to a file.
        """
        write_mfi2_btod3(filename,self.tod_dicc,overwrite=True)

# %% -------  PLOTTING ------------------------------

    def plot(self,galeq='galactic'):
        """

        Plots the coordinates of the horns in Galactic or Equatorial coordinates.

        Parameters
        ----------
        galeq : str
            'galactic' or 'equatorial'.

        """

        plt.figure()

        plt.subplot(2,2,1)
        labs = ['Horn 1','Horn 2','Horn 3','Horn 4']
        for i in range(4):
            if galeq == 'galactic':
                plt.plot(self.coord[:,i].l,
                         self.coord[:,i].b,label=labs[i])
            elif galeq == 'equatorial':
                plt.plot(self.coord[:,i].icrs.ra,
                         self.coord[:,i].icrs.dec,label=labs[i])
        if galeq == 'galactic':
            plt.xlabel('Galactic l [deg]')
            plt.ylabel('Galactic b [deg]')
        elif galeq == 'equatorial':
            plt.xlabel('RA [deg]')
            plt.ylabel('Dec [deg]')
        plt.legend()

        plt.subplot(2,2,2)
        plt.scatter(self.az,self.el,marker='.',s=1)
        plt.xlabel('Azimuth [deg]')
        plt.ylabel('Elevation [deg]')
        plt.axis('tight')

        plt.subplot(2,2,3)
        plt.plot(self.seconds,self.az)
        plt.xlabel('Observation time [s]')
        plt.ylabel('Azimuth [deg]')
        plt.axis('tight')

        plt.subplot(2,2,4)
        plt.plot(self.seconds,self.el)
        plt.xlabel('Observation time [s]')
        plt.ylabel('Elevation [deg]')
        plt.axis('tight')

    def plot_chan(self,ichan,flag=None,threshold=None,newfig=True,xaxis='mjd'):
        """
        Plots a single channel from a BTOD object.

        Parameters
        ----------
        btod : BTOD
            BTOD object to plot the channel from.
        ichan : int
            Channel index.
        flag : int, optional
            Flag value to mark. Default is None.
        threshold : float, optional
            Threshold value to mark. Default is None.
        newfig : bool, optional
            If True, creates a new figure. Default is True.
        xaxis : str, optional
            X-axis label. Default is 'mjd'.

        """

        if newfig:
            plt.figure()
        if xaxis=='mjd':
            x = self.mjd
        else:
            x = np.arange(0,self.size)
        y = self.data[:,ichan]
        plt.plot(x,y)
        if flag is not None:
            I = np.where(self.flag[:,ichan]==flag)
            plt.scatter(x[I],y[I],color='r',marker='*')
        if threshold is not None:
            z = np.ones(x.size)*(threshold*y.std()+y.mean())
            plt.plot(x,z,':g')
        if xaxis=='mjd':
            plt.xlabel('Modified Julian Date')
        else:
            plt.xlabel('TOD index')
        plt.ylabel('TOD')
        plt.axis('tight')

# %% -------  DETECTION ------------------------------

    def detect_thr(self,ichan,sigma_threshold,
                   toplot=False,closeplots=False):
        """
        Detects the indexes of the data that exceed a threshold.

        Parameters
        ----------
        ichan : int
            Channel index.
        sigma_threshold : float
            Threshold value in sigma units.
        toplot : bool, optional
            If True, plots the detected indexes. Default is False.
        closeplots : bool, optional
            If True, closes all previous plot windows. Default is False.

        Returns
        -------
        array-like
            Array with the indexes that exceed the threshold.
        """

        x = self.seconds
        y = self.data[:,ichan]

        indexes = detect_threshold(x,y,sigma_threshold,
                                   toplot=toplot,
                                   closeplots=closeplots)

        return indexes

# %% -------  FLAGGING ------------------------------

    def add_flags(self,output_flag_value,
                  sigma_threshold = 3.0,
                  use_filter      = True,
                  mask_flagged    = True,
                  flag_value      = 'all',
                  nbins           = 500,
                  tosmooth        = False,
                  lensmooth       = 100):
        """
        Flags the data that exceeds a threshold. The flagged data can be
        interpolated and/or filtered before applying the threshold. Flagging
        is done for each channel independently.

        Parameters
        ----------
        output_flag_value : int
            Flag value to assign to the flagged data.
        sigma_threshold : float, optional
            Threshold value in sigma units. Points above this threshold
            will be flagged with the vale set in the argument output_flag_value.
            Default is 3.0.
        use_filter : bool, optional
            If True, the data is filtered before applying the threshold.
            By default, the input data for the filter is a version of the
            data with the already flagged samples replaced by interpolated data.
            Default is True.
        mask_flagged : bool, optional
            If True, the flagged data is masked out and replaced by
            interpolated data from the non-masked data. Default is True.
        flag_value : int, optional
            Flag value that defines where to interpolate.
            If flag_value=='all', any flag value > 0 will be considered for the
            interpolation. Default is 'all'.
        nbins : int, optional
            Number of bins for the matched filter. Default is 500.
        tosmooth : bool, optional
            If True, the matched filter signal is smoothed. Default is False.
        lensmooth : int, optional
            Length of the smoothing window. Default is 100.

        Returns
        -------
        BTOD3
            A new BTOD3 object with the flagged data.
        """

        if use_filter:
            flagged = self.match_filter(mask_flagged=True,
                                        flag_value = flag_value,
                                        nbins      = nbins,
                                        tosmooth   = tosmooth,
                                        lensmooth  = lensmooth)
        elif mask_flagged:
            flagged = self.interp_data(flag_value=flag_value)
        else:
            flagged = self.copy()

        for ichan in range(flagged.nchans):

            dx = int(1.0*(flagged.fwhm(ichan)/flagged.azimuth_step).si.value)
            ix = flagged.detect_thr(ichan, sigma_threshold)

            for i in ix:
                a = np.max([0,i-dx])
                b = np.min([self.size,i+dx])
                flagged.tod_dicc['FLAG'][a:b,ichan] = output_flag_value

        non_filtered = self.copy()
        non_filtered.tod_dicc['FLAG'] = flagged.tod_dicc['FLAG']

        if use_filter:
            return {'original':non_filtered,
                    'filtered':flagged}
        else:
            return non_filtered



# %%   BTOD class

class BTOD:

    """
    A class to represent the BTOD (Basic Time-Ordered Data).

    Attributes
    ----------
    obstime : array-like
        Observation time as an array of astropy Time objects.
    az : array-like
        Azimuth angle as an array of scalars.
    el : array-like
        Elevation angle as an array of scalars.
    coords : array-like
        Array of SkyCoord objects representing sky coordinates.
    par : array-like
        Parallactic angle as an array.
    data : array-like
        Time-ordered data as a [Ns, Nch] array.
    mod_ang : array-like
        Modulator angle as a [Ns, Nch] array.
    wei : array-like
        Data weights as a [Ns, Nch] array.
    flag : array-like
        Flags for the data as a [Ns, Nch] array.
    gains : array-like
        Gain factors as an array.
    mod_ref_ang : array-like
        Reference angle for the modulator as an array.
    msbin : int
        Number of original time samples per BTOD binned sample.
    stype : array-like
        Array describing the type of signal contained in each channel.
    freq : array-like
        Array containing the central frequency of each channel.
    fwhm : array-like
        Array containing the FWHM value of each channel.

    Methods
    -------
    size():
        Returns the number of samples in the BTOD.
    copy():
        Returns a copy of the BTOD object.
    jd():
        Returns the Julian date of the observation.
    mjd():
        Returns the Modified Julian Date of the observation.
    seconds():
        Returns the observation time in seconds since the start.
    chan_mask(ichan):
        Masks out elements where BTOD.flag[chan] != 0 for a given channel.
    azimuth_step():
        Calculates the average absolute azimuth step between samples.
    """

    def __init__(self,obstime,az,el,coords,par,data,
                 mod_ang,wei,flag,gains,mod_ref_ang,msbin,stype,freq,fwhm):

        self.obstime     = obstime
        self.az          = az
        self.el          = el
        self.coords      = coords
        self.par         = par
        self.data        = data
        self.mod_ang     = mod_ang
        self.wei         = wei
        self.flag        = flag
        self.gains       = gains
        self.mod_ref_ang = mod_ref_ang
        self.msbin       = msbin
        self.stype       = stype
        self.freq        = freq
        self.fwhm        = fwhm

    def __getitem__(self,sliced):
        return BTOD(self.obstime[sliced],self.az[sliced],
                    self.el[sliced],self.coords[sliced],
                    self.par[sliced],self.data[sliced],
                    self.mod_ang[sliced],self.wei[sliced],
                    self.flag[sliced],self.gains,
                    self.mod_ref_ang,self.msbin,
                    self.stype,self.freq,self.fwhm)

    def size(self):
        """
        Returns the number of samples in the BTOD.

        Returns
        -------
        int
            Number of samples in the BTOD.
        """
        return self.az.size

    def copy(self):
        """
        Returns a copy of the BTOD object.

        Returns
        -------
        BTOD
            A copy of the BTOD object.
        """
        return BTOD(self.obstime,self.az,self.el,self.coords,
                    self.par,self.data,self.mod_ang,self.wei,
                    self.flag,self.gains,self.mod_ref_ang,
                    self.msbin,self.stype,self.freq,self.fwhm)

    def jd(self):
        """
        Returns the Julian date of the observation.

        Returns
        -------
        float
            Julian date of the observation.
        """
        return self.obstime.jd

    def mjd(self):
        """
        Returns the Modified Julian Date of the observation.

        Returns
        -------
        float
            Modified Julian Date of the observation.
        """
        return self.obstime.mjd

    def seconds(self):
        """
        Returns the observation time in seconds since the start.

        Returns
        -------
        float
            Observation time in seconds since the start.
        """
        t = self.obstime.jd
        t = t-t[0]
        return t*u.day.to('second')

    def chan_mask(self,ichan):
        """
        Masks out elements where BTOD.flag[chan] != 0 for a given channel.

        Parameters
        ----------
        ichan : int
            Channel index.

        Returns
        -------
        BTOD
            A masked BTOD object.
        """
        F = self.flag[:,ichan]
        D = self.data[:,ichan]
        m = (F==0)&(np.abs(D)>0.0)
        g = self.copy()
        return g[m]

    def azimuth_step(self):
        """
        Calculates the average absolute azimuth step between samples.

        Returns
        -------
        float
            Average absolute azimuth step in degrees.
        """
        azarr = np.array(self.az)
        daz   = azarr[1:]-azarr[:-1]
        npos  = np.count_nonzero(daz>0)
        nneg  = np.count_nonzero(daz<0)
        x     = 1e80
        y     = 1e80
        if npos>0:
            pdaz  = daz[daz>0]
            x     = np.abs(pdaz.mean())
        if nneg>0:
            ndaz  = daz[daz<0]
            y     = np.abs(ndaz.mean())
        z = np.min([x,y])
        return u.deg*z


# %%   AZ_STACK class

class AZ_STACK:
    """

    A class to represent the AZ_STACK data.

    Attributes
    ----------
    az : array-like
        Azimuth angle as an array of scalars.
    stack_k : array-like
        Stacked data as a [Ns, Nch] array.
    stacksmth_k : array-like
        Smoothed stacked data as a [Ns, Nch] array.
    stack_wmap : array-like
        Weight map as a [Ns, Nch] array.
    nstack : array-like
        Number of stacked samples as a [Ns, Nch] array.
    stack_noflag_k : array-like
        Stacked data without flagged samples as a [Ns, Nch] array.
    stacksmth_noflag_k : array-like
        Smoothed stacked data without flagged samples as a [Ns, Nch] array.
    stack_noflag_wmap : array-like
        Weight map without flagged samples as a [Ns, Nch] array.
    stack_noflag : array-like
        Number of stacked samples without flagged samples as a [Ns, Nch] array.
    gains : array-like
        Gain factors as an array.
    stype : array-like
        Array describing the type of signal contained in each channel.
    freq : array-like
        Array containing the central frequency of each channel.
    fwhm : array-like
        Array containing the FWHM value of each channel.

    Methods
    -------
    size():
        Returns the number of samples in the AZ_STACK.
    copy():
        Returns a copy of the AZ_STACK object.
    chan_mask(ichan):
        Masks out elements where AZ_STACK.stack_k[chan] != 0 for a given channel.
    azimuth_step():
        Calculates the average absolute azimuth step between samples.

    """

    def __init__(self,az,stack_k,stacksmth_k,stack_wmap,nstack,
                 stack_noflag_k,stacksmth_noflag_k,
                 stack_noflag_wmap,stack_noflag,
                 gains,stype,freq,fwhm):

        self.az                     = az
        self.stack_k                = stack_k
        self.stacksmth_k            = stacksmth_k
        self.stack_wmap             = stack_wmap
        self.nstack                 = nstack
        self.stack_noflag_k         = stack_noflag_k
        self.stacksmth_noflag_k     = stacksmth_noflag_k
        self.stack_noflag_wmap      = stack_noflag_wmap
        self.stack_noflag           = stack_noflag
        self.gains                  = gains
        self.stype                  = stype
        self.freq                   = freq
        self.fwhm                   = fwhm

    def __getitem__(self,sliced):
        return AZ_STACK(self.az[sliced],self.stack_k[sliced],
                        self.stacksmth_k[sliced],
                        self.stack_wmap[sliced],self.nstack[sliced],
                        self.stack_noflag_k[sliced],
                        self.stacksmth_noflag_k[sliced],
                        self.stack_noflag_wmap[sliced],
                        self.stack_noflag[sliced],
                        self.gains,self.stype,self.freq,self.fwhm)

    def copy(self):
        """
        Returns a copy of the AZ_STACK object.

        Returns
        -------
        AZ_STACK
            A copy of the AZ_STACK object.
        """
        return AZ_STACK(self.az,self.stack_k,self.stacksmth_k,
                        self.stack_wmap,
                        self.nstack,self.stack_noflag_k,
                        self.stacksmth_noflag_k,
                        self.stack_noflag_wmap,self.stack_noflag,
                        self.gains,self.stype,self.freq,self.fwhm)

    def size(self):
        """
        Returns the number of samples in the AZ_STACK.

        Returns
        -------
        int
            Number of samples in the AZ_STACK.
        """
        return self.az.size

    def chan_mask(self,ichan):
        """
        Masks out elements where AZ_STACK.stack_k[chan] != 0 for a given channel.

        Parameters
        ----------
        ichan : int
            Channel index.

        Returns
        -------
        AZ_STACK
            A masked AZ_STACK object.
        """
        D = self.stack_k[:,ichan]
        m = np.abs(D)>0.0
        g = self.copy()
        return g[m]

    def azimuth_step(self):
        """
        Calculates the average absolute azimuth step between samples.

        Returns
        -------
        float
            Average absolute azimuth step in degrees.
        """
        azarr = np.array(self.az)
        daz   = azarr[1:]-azarr[:-1]
        npos  = np.count_nonzero(daz>0)
        nneg  = np.count_nonzero(daz<0)
        x     = 1e80
        y     = 1e80
        if npos>0:
            pdaz  = daz[daz>0]
            x     = np.abs(pdaz.mean())
        if nneg>0:
            ndaz  = daz[daz<0]
            y     = np.abs(ndaz.mean())
        z = np.min([x,y])
        return u.deg*z


# %%   INPUT/OUTPUT ROUTINES

def get_btod(fname):
    """
    Reads a BTOD from a FITS file.

    Parameters
    ----------
    fname : str
        Path to the FITS file.

    Returns
    -------
    BTOD
        BTOD object containing the data from the FITS file.
    """

    hbtod = fits.open(fname)
    btod  = hbtod[1].data

    bjd   = btod[0][0]     # julian date
    baz   = btod[0][1]     # az
    bel   = btod[0][2]     # el
    bglon = btod[0][3]     # galactic lon, degrees
    bglat = btod[0][4]     # galactic lat, degrees
    bpar  = btod[0][5]     # paralactic angle, degrees
    bdata = btod[0][6]     # data
    bma   = btod[0][7]     # modulator angle, degrees
    bw    = btod[0][8]     # weight
    bflag = btod[0][9]     # flag
    bgain = btod[0][10]    # gain
    bmref = btod[0][11]    # reference angle of the modulator, degrees
    bmsb  = btod[0][12]    # msbin

    hbtod.close()

    bcoord = SkyCoord(l=bglon,b=bglat,unit=u.deg,frame='galactic')
    otime  = Time(bjd,format='jd')

    stype     = []
    farray    = []
    fwhmarray = []

    for i in range(32):
        stype.append(MFI_data['Signal'][i])
        farray.append(MFI_data['Frequency'][i])
        fwhmarray.append(MFI_data['FWHM'][i]*u.arcmin)

    return BTOD(otime,baz,bel,bcoord,bpar,bdata,bma,bw,
                bflag,bgain,bmref,bmsb,stype,farray,fwhmarray)

def write_btod(btod,fname):
    """
    Writes a BTOD to a FITS file.

    Parameters
    ----------
    btod : BTOD
        BTOD object to write to the FITS file.
    fname : str
        Path to the FITS file.
    """

    n     = btod.size()
    col1  = fits.Column(name='JD',format=str(n)+'D',array=[btod.jd()])
    col2  = fits.Column(name='AZ',format=str(n)+'E',array=[btod.az])
    col3  = fits.Column(name='EL',format=str(n)+'E',array=[btod.el])
    c     = btod.coords
    a     = c.l.value
    col4  = fits.Column(name='GL',format=str(a.size)+'E',
                        dim=str(a.transpose().shape),array=[a])
    a     = c.b.value
    col5  = fits.Column(name='GB',format=str(a.size)+'E',
                        dim=str(a.transpose().shape),array=[a])
    a     = btod.par
    col6  = fits.Column(name='PAR',format=str(a.size)+'E',
                        dim=str(a.transpose().shape),array=[a])
    a     = btod.data
    col7  = fits.Column(name='DATA',format=str(a.size)+'E',
                        dim=str(a.transpose().shape),array=[a])
    a     = btod.mod_ang
    col8  = fits.Column(name='MOD_ANG',format=str(a.size)+'E',
                        dim=str(a.transpose().shape),array=[a])
    a     = btod.wei
    col9  = fits.Column(name='WEI',format=str(a.size)+'E',
                        dim=str(a.transpose().shape),array=[a])
    a     = btod.flag
    col10 = fits.Column(name='FLAG',format=str(a.size)+'I',
                        dim=str(a.transpose().shape),array=[a])
    a     = btod.gains
    col11 = fits.Column(name='GAINS',format=str(a.size)+'E',array=[a])
    a     = btod.mod_ref_ang
    col12 = fits.Column(name='MOD_REF_ANGLE',format=str(a.size)+'E',array=[a])
    col13 = fits.Column(name='MSBIN',format='E',array=[btod.msbin])

    cols = [col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12,col13]
    tbhdu = fits.BinTableHDU.from_columns(cols)
    tbhdu.writeto(fname,clobber=True)

def get_stod(fname):
    """
    Reads an AZ_STACK from a FITS file.

    Parameters
    ----------
    fname : str
        Path to the FITS file.

    Returns
    -------
    AZ_STACK
        AZ_STACK object containing the data from the FITS file.
    """

    hbtod = fits.open(fname)
    btod  = hbtod[1].data

    az                     = btod[0][0]*u.deg
    stack_k                = btod[0][1]
    stacksmth_k            = btod[0][2]
    stack_wmap             = btod[0][3]
    nstack                 = btod[0][4]
    stack_noflag_k         = btod[0][5]
    stacksmth_noflag_k     = btod[0][6]
    stack_noflag_wmap      = btod[0][7]
    stack_noflag           = btod[0][8]
    gains                  = btod[0][9]

    hbtod.close()

    stype     = []
    farray    = []
    fwhm      = []
    for i in range(32):
        stype.append(MFI_data['Signal'][i])
        farray.append(MFI_data['Frequency'][i])
        fwhm.append(MFI_data['FWHM'][i]*u.arcmin)

    return AZ_STACK(az,stack_k,stacksmth_k,stack_wmap,nstack,
                    stack_noflag_k,stacksmth_noflag_k,
                    stack_noflag_wmap,stack_noflag,
                    gains,stype,farray,fwhm)

# %%   BTOD AND AZ_STACK INTERPOLATION


def interpolate_btod(btod_input,npoints,endpoints=None):
    """

    Interpolates a BTOD object to a new number of points.

    Parameters
    ----------
    btod_input : BTOD
        BTOD object to interpolate.
    npoints : int
        Number of points to interpolate to.
    endpoints : array-like, optional
        Array of two elements with the start and end points of the interpolation.
        Default is None.

    Returns
    -------
    BTOD
        Interpolated BTOD object.
    """

    t0 = btod_input.jd()
    if endpoints is not None:
        t = np.linspace(endpoints[0],endpoints[1],npoints,dtype='float64')
    else:
        t = np.linspace(t0.min(),t0.max(),npoints,dtype='float64')

    az0 = btod_input.az
    f   = interp1d(t0,az0,bounds_error=False,fill_value='extrapolate')
    az  = f(t)

    el0 = btod_input.el
    f   = interp1d(t0,el0,bounds_error=False,fill_value='extrapolate')
    el  = f(t)

    l0 = btod_input.coords[:,:].l.deg
    b0 = btod_input.coords[:,:].b.deg
    f  = interp1d(t0,l0,axis=0,bounds_error=False,fill_value=0.0)
    g  = interp1d(t0,b0,axis=0,bounds_error=False,fill_value=0.0)
    cc = SkyCoord(l=f(t),b=g(t),unit=u.deg,frame='galactic')

    f  = interp1d(t0,btod_input.par,axis=0,
                  bounds_error=False,fill_value='extrapolate')
    pp = f(t)
    f  = interp1d(t0,btod_input.data,axis=0,
                  bounds_error=False,fill_value='extrapolate')
    dd = f(t)
    f  = interp1d(t0,btod_input.mod_ang,axis=0,
                  bounds_error=False,fill_value='extrapolate')
    mm = f(t)
    f  = interp1d(t0,btod_input.wei,axis=0,
                  bounds_error=False,fill_value='extrapolate')
    ww = f(t)
    f  = interp1d(t0,btod_input.flag,axis=0,
                  bounds_error=False,fill_value='extrapolate')
    fl = f(t)

    btod_out = BTOD(Time(t,format='jd'),az,el,cc,pp,dd,mm,ww,fl,
                    btod_input.gains,btod_input.mod_ref_ang,
                    btod_input.msbin,btod_input.stype,
                    btod_input.freq,btod_input.fwhm)

    return btod_out

def interpolate_btod3_dict(btod_input,interp_btod,dicc_input):
    """

    Interpolates a BTOD3 dictionary from a btod3 and its corresponding
    interpolated btod3. The new interpolated dictionary has the same
    sampling as the interpolated btod3 object.

    Parameters
    ----------
    btod_input : BTOD
        BTOD object before interpolation.
    interp_btod : BTOD
        BTOD object after interpolation.
    dicc_input : dict
        Associated btod3 dictionary before interpolation
        (see read_mfi2_btod3() for additional info)

    Returns
    -------
    dict
        Interpolated associated btod3 dictionary

    """

    t0 = btod_input.obstime.value
    t  = interp_btod.obstime.value

    dicc_output = dicc_input.copy()

    for k in dicc_output.keys():
        typ = check_variable_type(dicc_output(k))
        if typ in ['scalar','string','unknown']:
            pass
        elif typ == 'array':
            y0  = dicc_output[k]
            f   = interp1d(t0,y0,
                           bounds_error=False,
                           fill_value='extrapolate')
            y   = f(t)
            dicc_output[k] = y
        elif typ == 'ndarray':
            y0  = dicc_output[k]
            f   = interp1d(t0,y0,axis=0,
                           bounds_error=False,
                           fill_value='extrapolate')
            y   = f(t)
            dicc_output[k] = y
        else:
            pass

    dicc_output['DATA'] = interp_btod.data

    return dicc_output

def interpolate_az_stack(stod_input,npoints):
    """

    Interpolates an AZ_STACK object to a new number of points.

    Parameters
    ----------
    stod_input : AZ_STACK
        AZ_STACK object to interpolate.
    npoints : int
        Number of points to interpolate to.

    Returns
    -------
    AZ_STACK
        Interpolated AZ_STACK object.

    """

    t0 = np.array(stod_input.az.value)
    t  = np.linspace(t0.min(),t0.max(),npoints)
    f  = interp1d(t0,stod_input.stack_k,axis=0)
    n1 = f(t)
    f  = interp1d(t0,stod_input.stacksmth_k,axis=0)
    n2 = f(t)
    f  = interp1d(t0,stod_input.stack_wmap,axis=0)
    n3 = f(t)
    f  = interp1d(t0,stod_input.nstack,axis=0)
    n4 = f(t)
    f  = interp1d(t0,stod_input.stack_noflag_k,axis=0)
    n5 = f(t)
    f  = interp1d(t0,stod_input.stacksmth_noflag_k,axis=0)
    n6 = f(t)
    f  = interp1d(t0,stod_input.stack_noflag_wmap,axis=0)
    n7 = f(t)
    f  = interp1d(t0,stod_input.stack_noflag,axis=0)
    n8 = f(t)

    stod_out = AZ_STACK(t*u.deg,n1,n2,n3,n4,n5,n6,n7,n8,stod_input.gains,
                        stod_input.stype,stod_input.freq,stod_input.fwhm)

    return stod_out

# %%   FILTER INDIVIDUAL CHANNELS

def extract_chan_btod(btod,ichan,toplot=False):
    """

    Extracts a single channel from a BTOD object.

    Parameters
    ----------
    btod : BTOD
        BTOD object to extract the channel from.
    ichan : int
        Channel index.
    toplot : bool, optional
        If True, plots the extracted channel. Default is False.

    Returns
    -------
    array-like
        Array of azimuth values.
    """
    N         = btod.size()
    jud       = btod.jd()
    if (np.count_nonzero(btod.flag[:,ichan]==0)/N) > 0.2:
        btod2     = btod.chan_mask(ichan)
        btod3     = interpolate_btod(btod2,N,endpoints=[jud[0],jud[N-1]])
    else:
        btod3     = btod.copy()
    x         = btod3.seconds()
    y         = btod3.data[:,ichan]
    uf        = btod3.fwhm[ichan]/btod3.azimuth_step()
    fwhm      = uf.si.value

    if toplot:
        plt.figure()
        plt.plot(x,y)
        plt.xlabel('AZ [deg]')
        plt.ylabel('stack K')
        plt.axis('tight')

    return x,y,fwhm

def extract_diff_chan_btod(btod,ichan,jchan,toplot=False):
    """

    Extracts the difference between two channels from a BTOD object.

    Parameters
    ----------
    btod : BTOD
        BTOD object to extract the channel from.
    ichan : int
        Channel index.
    jchan : int
        Channel index.
    toplot : bool, optional
        If True, plots the extracted channel. Default is False.

    Returns
    -------
    array-like
        Array of azimuth values.

    """
    N         = btod.size()
    jud       = btod.jd()
    if (np.count_nonzero(btod.flag[:,ichan]==0)/N) > 0.2:
        btod2     = btod.chan_mask(ichan)
        btod3     = interpolate_btod(btod2,N,endpoints=[jud[0],jud[N-1]])
    else:
        btod3     = btod.copy()
    x         = btod3.seconds()
    y         = btod3.data[:,ichan]-btod3.data[:,jchan]
    uf1       = btod3.fwhm[ichan]/btod3.azimuth_step()
    uf2       = btod3.fwhm[jchan]/btod3.azimuth_step()
    fwhm      = np.sqrt((uf1.si.value)**2+(uf2.si.value)**2)

    if toplot:
        plt.figure()
        plt.plot(x,y)
        plt.xlabel('AZ [deg]')
        plt.ylabel('stack K')
        plt.axis('tight')

    return x,y,fwhm

def filter_chan_btod(btod,ichan,nbins=500,toplot=False,
                     verbose=False,tosmooth=False,lensmooth=100):
    """

    Filters a single channel from a BTOD object.

    Parameters
    ----------
    btod : BTOD
        BTOD object to filter the channel from.
    ichan : int
        Channel index.
    nbins : int, optional
        Number of bins for the matched filter. Default is 500.
    toplot : bool, optional
        If True, plots the filtered channel. Default is False.
    verbose : bool, optional
        If True, prints information about the filter. Default is False.
    tosmooth : bool, optional
        If True, smooths the filtered channel. Default is False.
    lensmooth : int, optional
        Length of the smoothing window. Default is 100.

    Returns
    -------
    array-like
        Array of azimuth values.
    """

    x,y,fwhm = extract_chan_btod(btod,ichan)
    if verbose:
        print(' ')
        print(' ---- Estimated FWMH = {0} samples'.format(fwhm))
    fy       = mf1.matched_filter_1d(y,tprof=fwhm,nbins=nbins,topad=True,
                                     toplot=toplot,verbose=verbose,
                                     tosmooth=tosmooth,lensmooth=lensmooth)
    return x,fy

def extract_chan_stod(stod,ichan,toplot=False):
    """

    Extracts a single channel from an AZ_STACK object.

    Parameters
    ----------
    stod : AZ_STACK
        AZ_STACK object to extract the channel from.
    ichan : int
        Channel index.
    toplot : bool, optional
        If True, plots the extracted channel. Default is False.

    Returns
    -------
    array-like
        Array of azimuth values.
    """

    N         = stod.size()
    mask      = stod.stacksmth_k[:,ichan]!=0
    stod2     = interpolate_az_stack(stod[mask],N)
    x         = stod2.az.value
    y         = stod2.stacksmth_k[:,ichan]
    uf        = stod2.fwhm[ichan]/stod2.azimuth_step()
    fwhm      = uf.si.value

    if toplot:
        plt.figure()
        plt.plot(x,y)
        plt.xlabel('AZ [deg]')
        plt.ylabel('stack K')
        plt.axis('tight')
    return x,y,fwhm

def filter_chan_stod(stod,ichan,nbins=150,toplot=False,
                     verbose=False,tosmooth=False,lensmooth=100):
    """

    Filters a single channel from an AZ_STACK object.

    Parameters
    ----------
    stod : AZ_STACK
        AZ_STACK object to filter the channel from.
    ichan : int
        Channel index.
    nbins : int, optional
        Number of bins for the matched filter. Default is 150.
    toplot : bool, optional
        If True, plots the filtered channel. Default is False.
    verbose : bool, optional
        If True, prints information about the filter. Default is False.
    tosmooth : bool, optional
        If True, smooths the filtered channel. Default is False.
    lensmooth : int, optional
        Length of the smoothing window. Default is 100.

    Returns
    -------
    array-like
        Array of azimuth values.
    """

    x,y,fwhm = extract_chan_stod(stod,ichan)
    if verbose:
        print(' ')
        print(' ---- Estimated FWMH = {0} samples'.format(fwhm))
    fy       = mf1.matched_filter_1d(y,tprof=fwhm,nbins=nbins,
                                     topad=True,toplot=toplot,
                                     verbose=verbose,tosmooth=tosmooth,
                                     lensmooth=lensmooth)
    return x,fy

# %%   FILTER WHOLE BTODS AND AZ_STACKS


def filter_btod(btod,nbins=500,tosmooth=False,lensmooth=100):
    """

    Filters a BTOD object.

    Parameters
    ----------
    btod : BTOD
        BTOD object to filter.
    nbins : int, optional
        Number of bins for the matched filter. Default is 500.
    tosmooth : bool, optional
        If True, smooths the filtered BTOD. Default is False.
    lensmooth : int, optional
        Length of the smoothing window. Default is 100.

    Returns
    -------
    BTOD
        Filtered BTOD object.
    """

    N     = btod.size()
    jud   = btod.jd()
    btod1 = interpolate_btod(btod,N,endpoints=[jud[0],jud[N-1]])
    M     = btod1.data.shape[1]
    for i in range(M):
        x,y = filter_chan_btod(btod1,i,nbins=nbins,tosmooth=tosmooth,
                               lensmooth=lensmooth)
        mask = btod.data[:,i]!=0
        for j in range(8):
            mask = grow_mask(mask)
        y[~mask] =0.0
        btod1.data[:,i] = (y-y.mean())/y.std()
    return btod1

def filter_stod(stod,nbins=150,tosmooth=False,lensmooth=100):
    """

    Filters an AZ_STACK object.

    Parameters
    ----------
    stod : AZ_STACK
        AZ_STACK object to filter.
    nbins : int, optional
        Number of bins for the matched filter. Default is 150.
    tosmooth : bool, optional
        If True, smooths the filtered AZ_STACK. Default is False.
    lensmooth : int, optional
        Length of the smoothing window. Default is 100.

    Returns
    -------
    AZ_STACK
        Filtered AZ_STACK object.
    """

    fstod0 = stod.copy()
    N      = fstod0.size()
    fstod1 = interpolate_az_stack(fstod0,N)
    M      = fstod1.stacksmth_k.shape[1]
    fstack = fstod1.stacksmth_k
    for i in range(M):
        x,y  = filter_chan_stod(fstod1,i,nbins=nbins,
                                tosmooth=tosmooth,
                                lensmooth=lensmooth)
        mask = stod.stacksmth_k[:,i]!=0
        for j in range(12):
            mask = grow_mask(mask)
        y[~mask] = 0.0
        fstack[:,i] = y/y.std()
    fstod1.stacksmth_k = fstack
    return fstod1


# %%   MISCELLANEA

def clos():
    """
    Closes all open plots.
    """

    plt.close('all')

def grow_mask(mask):
    """
    Grows a mask by one element.

    Parameters
    ----------
    mask : array-like
        Mask to grow.

    Returns
    -------
    array-like
        Grown mask.
    """

    items  = deque(mask)
    centro = [x for x in items]
    items.rotate(1)
    dcha   = [x for x in items]
    items.rotate(-2)
    izqd   = [x for x in items]
    mask1  = np.multiply(centro,dcha)
    mask2  = np.multiply(mask1,izqd)
    return mask2

def find_nearest(array,value):
    """
    Finds the nearest value in an array.

    Parameters
    ----------
    array : array-like
        Array to search.
    value : float
        Value to find the nearest to.

    Returns
    -------
    int
        Index of the nearest value in the array.
    """
    x = np.abs(array-value)
    I = np.argmin(x)
    if I.size>1:
        r = I[0]
    else:
        r = I
    return r


def info_canal(ichan):
    """
    Prints information about a channel.

    Parameters
    ----------
    ichan : int
        Channel index.

    """

    print(' ')
    print(' --- Horn number: {0}'.format(MFI_data['Polarizer'][MFI_data['Channel']==ichan][0]))
    print(' --- Signal type: {0}'.format(MFI_data['Signal'][MFI_data['Channel']==ichan][0]))
    print(' --- Central frequency: {0} GHz'.format(MFI_data['Frequency'][MFI_data['Channel']==ichan][0]))
    print(' --- Channel reference name: {0}'.format(MFI_data['Channel name'][MFI_data['Channel']==ichan][0]))


def check_variable_type(v):
    """
    Determines the type of the given variable.

    Parameters
    ----------
    v : any
        The variable to check.

    Returns
    -------
    str
        The type of the variable, which can be:
        - 'string' if v is a string.
        - 'scalar' if v is a numeric scalar.
        - 'array' if v is a 1-dimensional numpy array.
        - 'ndarray' if v is a 2 or more dimensional numpy array.
        - 'unknown' if the type of v is not recognized.

    Examples
    --------
    >>> check_variable_type("Hello")
    'string'
    >>> check_variable_type(3.14)
    'scalar'
    >>> check_variable_type(np.array([1, 2, 3]))
    'array'
    >>> check_variable_type(np.array([[1, 2], [3, 4]]))
    'ndarray'
    >>> check_variable_type({"key": "value"})
    'unknown'
    """
    if isinstance(v, str):
        return 'string'
    elif isinstance(v, (int, float, complex)):
        return 'scalar'
    elif type(v) == np.float32:
        return 'scalar'
    elif isinstance(v, np.ndarray):
        if v.ndim == 1:
            return 'array'
        elif v.ndim >= 2:
            return 'ndarray'
    else:
        return 'unknown'


# %%   PLOTTING

def plot_btod_coords(btod):
    """

    Plots the coordinates of a BTOD object.

    Parameters
    ----------
    btod : BTOD
        BTOD object to plot.

    """

    plt.figure()

    plt.subplot(2,2,1)
    labs = ['Horn 1','Horn 2','Horn 3','Horn 4']
    for i in range(4):
        plt.plot(btod.coords[:,i].l,btod.coords[:,i].b,label=labs[i])
    plt.xlabel('Galactic l [deg]')
    plt.ylabel('Galactic b [deg]')
    plt.legend()

    plt.subplot(2,2,2)
    plt.plot(btod.az,btod.el)
    plt.xlabel('Azimuth [deg]')
    plt.ylabel('Elevation [deg]')
    plt.axis('tight')

    plt.subplot(2,2,3)
    plt.plot(btod.seconds(),btod.az)
    plt.xlabel('Observation time [s]')
    plt.ylabel('Azimuth [deg]')
    plt.axis('tight')

    plt.subplot(2,2,4)
    plt.plot(btod.seconds(),btod.el)
    plt.xlabel('Observation time [s]')
    plt.ylabel('Elevation [deg]')
    plt.axis('tight')

def plot_btod_chan(btod,ichan,flag=None,threshold=None,newfig=True,xaxis='mjd'):
    """
    Plots a single channel from a BTOD object.

    Parameters
    ----------
    btod : BTOD
        BTOD object to plot the channel from.
    ichan : int
        Channel index.
    flag : int, optional
        Flag value to mark. Default is None.
    threshold : float, optional
        Threshold value to mark. Default is None.
    newfig : bool, optional
        If True, creates a new figure. Default is True.
    xaxis : str, optional
        X-axis label. Default is 'mjd'.

    """

    if newfig:
        plt.figure()
    if xaxis=='mjd':
        x = btod.mjd()
    else:
        x = np.arange(0,btod.size())
    y = btod.data[:,ichan]
    plt.plot(x,y)
    if flag is not None:
        I = np.where(btod.flag[:,ichan]==flag)
        plt.scatter(x[I],y[I],color='r',marker='*')
    if threshold is not None:
        z = np.ones(x.size)*(threshold*y.std()+y.mean())
        plt.plot(x,z,':g')
    if xaxis=='mjd':
        plt.xlabel('Modified Julian Date')
    else:
        plt.xlabel('TOD index')
    plt.ylabel('TOD')
    plt.axis('tight')


# %%   DETECTION OVER A THRESHOLD

def detect_threshold(x,y,sigma_threshold,toplot=False,closeplots=False):
    """
    Detects peaks over a threshold in a 1D array.

    Parameters
    ----------
    x : array-like
        Array of x values.
    y : array-like
        Array of y values.
    sigma_threshold : float
        Threshold value.
    toplot : bool, optional
        If True, plots the detected peaks. Default is False.
    closeplots : bool, optional
        If True, closes all open plots. Default is False.

    Returns
    -------
    array-like
        Array of peak indexes.
    """

    y2 = pu.prepare.scale(y)[0]
    relative_threshold = y2.mean()+sigma_threshold*y2.std()
    indexes = pu.indexes(y,thres=relative_threshold,min_dist=10)
    if toplot:
        if closeplots:
            plt.close('all')
        plt.figure()
        pplot(x,y,indexes)
        plt.plot(x,np.ones(np.size(x))*sigma_threshold*y.std(),':r')
    return indexes


# %%   FLAGGING

def flag_BTOD(input_btod_file,output_btod_file,threshold=5.0,flag_value=-1):

    input_btod  = get_btod(input_btod_file)
    jd0         = input_btod.jd()
    AZstep      = input_btod.azimuth_step()
    output_btod = input_btod.copy()

    ftod        = filter_btod(output_btod)

                  # Filters the BTOD. For this filtering step,
                  #   each channel is filtered individually with
                  #   a 1D matched filter adapted to the FWHM
                  #   of the MFI at that channel. Prior to this,
                  #   the flagged data are removed and substituted
                  #   with a linear interpolation of the non-flagged
                  #   data around.

    jdf         = ftod.jd()
    x           = jdf.copy()

    for channel in range(ftod.data.shape[1]):

        if (np.count_nonzero(ftod.flag[:,channel]==0)/ftod.size()) > 0.2:

            stp            = input_btod.fwhm[channel]/AZstep
            fwhm           = stp.si.value
            three_fwhm     = np.int(3*fwhm)+1

            y              = ftod.data[:,channel]
            peak_locations = detect_threshold(x,y,sigma_threshold=threshold)
            npeaks         = np.size(peak_locations)

            if npeaks>0:
                for pico in range(npeaks):
                    i1 = peak_locations[pico]-three_fwhm
                    i1 = np.max((0,i1))
                    i2 = peak_locations[pico]+three_fwhm+1
                    i2 = np.min((i2,output_btod.size()))
                    ftod.flag[i1:i2,channel] = flag_value
                    for pixel in range(i1,i2):
                        val   = jdf[pixel]
                        pos   = find_nearest(jd0,val)
                        output_btod.flag[pos,channel] = flag_value

    write_btod(output_btod,output_btod_file)

    input_btod  = get_btod(input_btod_file)

def flag_AZ_STACK(input_stod_file,output_flag_file,threshold=5.0,flag_value=-1):
    """

    Flags an AZ_STACK object.

    Parameters
    ----------
    input_stod_file : str
        Path to the input AZ_STACK file.
    output_flag_file : str
        Path to the output flag file.
    threshold : float, optional
        Threshold value. Default is 5.0.
    flag_value : int, optional
        Flag value. Default is -1.

    """

    input_stod  = get_stod(input_stod_file)
    output_stod = input_stod.copy()
    flags       = np.zeros(shape=output_stod.stacksmth_k.shape)
    ncols       = input_stod.stacksmth_k.shape[1]
    cols = []

    fstod       = filter_stod(output_stod)

                  # Filters AZ_STACK array. For this filtering step,
                  #   each channel is filtered individually with
                  #   a 1D matched filter adapted to the FWHM
                  #   of the MFI at that channel. Prior to this,
                  #   the flagged data are removed and substituted
                  #   with a linear interpolation of the non-flagged
                  #   data around.

    x = fstod.az.value

    for channel in range(ncols):

        stp            = input_stod.fwhm[channel]/input_stod.azimuth_step()
        fwhm           = stp.si.value
        three_fwhm     = np.int(3*fwhm)+1

        y              = fstod.stacksmth_k[:,channel]
        peak_locations = detect_threshold(x,y,sigma_threshold=threshold)
        npeaks         = np.size(peak_locations)

        if npeaks>0:
            for pico in range(npeaks):
                i1 = peak_locations[pico]-three_fwhm
                i1 = np.max((0,i1))
                i2 = peak_locations[pico]+three_fwhm+1
                i2 = np.min((i2,input_stod.size()))
                flags[i1:i2,channel] = flag_value

        colname = str(np.int(channel+1))
        cflag   = fits.Column(name=colname,format='J',array=flags[:,channel])
        cols.append(cflag)

    tbhdu = fits.BinTableHDU.from_columns(cols)
    tbhdu.writeto(output_flag_file,clobber=True)

