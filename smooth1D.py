#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 22 17:15:26 2016

@author: herranz
"""

import numpy as np

def smooth(x,window_len=11,window='hanning'):
    """
    Smoothes the input signal using a specified window type and length.

    Parameters
    ----------
    signal : array-like
        The input signal to be smoothed.
    window_len : int, optional
        The dimension of the smoothing window; it should be an odd integer.
        Default is 11.
    window : str, optional
        The type of window to use for smoothing. Options are 'flat',
        'hanning', 'hamming', 'bartlett', 'blackman'.
        Default is 'hanning'.

    Returns
    -------
    numpy.ndarray
        The smoothed signal.

    Raises
    ------
    ValueError
        If the input signal is not one-dimensional or if window_len is less than 3.

    Examples
    --------
    >>> import numpy as np
    >>> signal = np.random.random(100)
    >>> smoothed_signal = smooth_signal(signal, window_len=11, window='hanning')
    """

    if x.ndim != 1:
        raise ValueError("smooth only accepts 1 dimension arrays.")

    if x.size < window_len:
        raise ValueError("Input vector needs to be bigger than window size.")


    if window_len<3:
        return x


    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise(ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")


    s=np.r_[x[window_len-1:0:-1],x,x[-1:-window_len:-1]]
    #print(len(s))
    if window == 'flat': #moving average
        w=np.ones(window_len,'d')
    else:
        w=eval('np.'+window+'(window_len)')

    y=np.convolve(w/w.sum(),s,mode='valid')
    return y[window_len//2:-(window_len//2)]




